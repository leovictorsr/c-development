/**
 * Nome: Leonardo Victor da Silva Rodrigues
 * RA: 172109
 * Laboratorio 01 - O problema do acesso a lista
 */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>

/* Constantes */
#define SUCCESS 1
#define FAILURE 0

 /* Structs */
 /* Struct para No */
 typedef struct Node {
    int el;
    struct Node* previous;
    struct Node* next;
 } Node;

/* Struct para Lista */
 typedef struct List{
    struct Node* head;
 } List;


/* Prototipo de funcoes */
List* createList ();
Node* createNode ();
void insertNodeAtEnd (List* l, int key);
void insertNodeAtStart (List* l, int key);
int isListEmpty (List* l);
void printList (List* l);


/** 
 * Eu sou a função principal do programa.
 * Eu chamo as funções que gerenciam a lista.
 */
int main ()
{
    /* Variaveis locais */
    int tam, requests, key, i;
    Node* node;
    List* list;

    /* Le o numero de elementos e de requisicoes */
    scanf("%d %d", &tam, &requests);

    /* Cria a lista */
    list = createList();

    /* Cria os nós da lista */
    for (i = 0; i < tam; i++)
    {
        scanf("%d", &key);
        insertNodeAtEnd(list, key);
    }

    printList(list);

    return 0;
}

/**
 * Eu crio uma lista.
 *
 * :retorna: a lista criada
 * :tipo de retorno: List*
 */
List* createList()
{
    return (List *) calloc(1, sizeof(List));
}

/**
 * Eu crio um nó.
 *
 * :retorna: o nó criado
 * :tipo de retorno: Node*
 */
Node* createNode()
{
    return (Node *) calloc(1, sizeof(Node));
}

/**
 * Eu checo se uma lista é vazia.
 *
 * :param l: a lista a ser checada
 * :type  l: List*
 *
 * :retorna: SUCCESS ou FAILURE
 * :tipo de retorno: int
 */
int isListEmpty(List* l)
{
    /* Lista é nula: retorna sucesso */
    if (l == NULL)
    {
        return SUCCESS;        
    }

    /* Nó cabeça é nulo: retorna sucesso */
    else if (l->head == NULL)
    {
        return SUCCESS;
    }

    /* Retorna falha */
    return FAILURE;
}

/**
 * Eu insiro um nó no final da lista.
 *
 * :param l: a lista onde será inserido o nó
 * :type  l: List*
 *
 * :param key: a chave a ser armazenada no nó
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void insertNodeAtEnd (List* l, int key)
{
    /* Pego o nó cabeça da lista */
    Node *position = l->head;

    /* Nó cabeça é nulo: insere o nó e retorna */
    if (position == NULL)
    {
        insertNodeAtStart(l, key);
        return;
    }

    /* Alcanço o final da lista */
    while (position->next != NULL)
    {
        position = position->next;
    }

    /* Crio o nó e armazeno a chave */
    Node* node = createNode();
    node->el = key;

    /* Posiciono o nó criado na lista */
    node->previous = position;
    position->next = node;
}

/**
 * Eu insiro um nó no começo da lista.
 *
 * :param l: a lista onde será inserido o nó
 * :type  l: List*
 *
 * :param key: a chave a ser armazenada no nó
 * :type  key: int
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void insertNodeAtStart (List* l, int key)
{
    /* Crio o nó e armazeno a chave */
    Node* n = createNode();
    n->el = key;

    /* Posiciono o nó na lista */
    n->next = l->head;

    /* A lista não é vazia: linka o nó criado com o nó cabeça da lista */
    if (!isListEmpty(l))
        l->head->previous = n;

    /* O nó criado se torna o nó cabeça */
    l->head = n;
}

/**
 * Eu imprimo uma lista.
 *
 * :param l: a lista a ser impressa
 * :type  l: List*
 *
 * :retorna: nada
 * :tipo de retorno: void
 */
void printList (List* l)
{
    /* Pego o nó cabeça */
    Node* n = l->head;

    /* Percorro a linha imprimindo os elementos */
    while (n != NULL)
    {
        printf("%d --> ", n->el);
        n = n->next;
    }
    printf("\n");
}
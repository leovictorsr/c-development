#include <stdio.h>

int main(){
  int bin, dec = 0, pot = 1, bit;
  printf("Insira um numero binario :: ");
  scanf("%d",&bin);
  printf("%d em base 2 = ",bin);
 
  while (bin>0){
    bit = bin % 10;
    bin = bin / 10;
    dec = dec + (bit * pot);
    pot = pot * 2;
  }
 
  printf("%d em base 10.\n",dec);
  return 0 ;
}

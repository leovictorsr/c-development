#include <stdio.h>
#include <stdlib.h>

/* Struct for node */
typedef struct Node {
    int key;
    struct Node* previous;
} Node;

/* Struct for stack */
typedef struct Stack {
    int size;
    struct Node* tail;
} Stack;

/* Functions prototypes */
Node* createNode();
Stack* createStack();
void destroyNode(Node* node);
void destroyStack(Stack* stack);
void pop (Stack* stack);
void printStack(Stack* stack);
void push (Stack* stack, int key);

int main ()
{
    // local variables
    int key;
    char option = ' ';
    Stack* stack;

    // create stack
    stack = createStack();

    return 0;
}

Node* createNode()
{
    return (Node *) calloc (1, sizeof(Node));
}

Stack* createStack()
{
    return (Stack *) calloc (1, sizeof(Stack));
}

void destroyStack(Stack* stack)
{
    // get stack tail
    Node* position = stack->tail;
    Node* aux = stack->tail->previous;

    while (aux != NULL)
    {
        free(position);
        position = aux;
        aux = aux->previous;
    }

    free(stack);
}

void pop (Stack* stack)
{
    // get stack tail
    Node* tail = stack->tail;

    // make previous the tail
    stack->tail = tail->previous;

    // delete former tail
    free(tail);
}

void printStack(Stack* stack)
{
    // get stack tail
    Node* position = stack->tail;

    // iterate by stack printing its nodes keys
    while (position != NULL)
    {
        printf("%d --> ", position->key);
        position = position->previous;
    }
    printf("\n");
}

void push (Stack* stack, int key)
{
    // get stack tail
    Node* tail = stack->tail;

    // create node
    Node* node = createNode();
    node->key = key;
    node->previous = tail;

    // make node the tail
    stack->tail = node;
}
